// pages/signup/index.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    array: ['门店0', '门店1', '门店2', '门店3'],
    array1: ['教师', '学生', '企业家', '工人'],
    array2:['1','2','3'],
    select: false,
    json: {}
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {

  },

  confirm() {
    var z=this 
    // let json = {
    //   name: '',
    //   number: null,
    //   shopname:null,
    //   profession: null,
    //   totalnum:0
    // }
    // let {
    //   num,
    // } = this.data
    // console.log(json)
    var json = this.data.json
    console.log(json)
    wx.navigateTo({
      // url: '/pages/signupconfirm/index?info='+JSON.stringify(json),
      url: '/pages/signupconfirm/index?info=' + JSON.stringify(json),
   
    })

  },
  bindPickerChange: function(e) {
    console.log('picker发送选择改变，携带值为', e)
    let json = this.data.json
    json['shopname'] = this.data.array[e.detail.value]
    console.log(this.data.json)
    this.setData({
      index: e.detail.value,
    })
  },
  bindPickerChange1: function (e) {
    console.log('picker发送选择改变，携带值为', e.detail.value)
    let json = this.data.json
    json['profession'] = this.data.array1[e.detail.value]
    console.log(this.data.json)
    this.setData({
      index1: e.detail.value
    })
  },
  bindPickerChange2: function (e) {
    console.log('picker发送选择改变，携带值为', e.detail.value)
    let json = this.data.json
    json['totalnum'] = this.data.array2[e.detail.value]
    console.log(this.data.json)
    this.setData({
      index2: e.detail.value
    })
  },
  onPageScroll(e) {
    console.log(e)
    this.selectComponent('#container').pageScrollHandler(e)
  },
  mySelect(e) {

    this.setData({

      select: false
    })
  },
  
  mySelectshop(e) {

    this.setData({
      selectshop: false
    })
  },
  
  bindShowMsg() {
    this.setData({
      select: !this.data.select
    })
  },
  bindShowMsgshop() {
    this.setData({
      selectshop: !this.data.selectshop
    })
  },
  bindShowMsgperson(){
    this.setData({
      selectperson: !this.data.selectperson
    })
  },
  inputChange(e) {
    console.log(e)
    var z=this
    let {
      value
    } = e.detail
    console.log(value)
    let {
      key
    } = e.currentTarget.dataset
    console.log(key)
    let json = this.data.json

    json[key] = value

    console.log(json)

    this.setData({
      json:json
    })
    console.log(this.data.json)
  },
})