// pages/promiseTest/index.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    weather:[],
    weatherip:'',
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {

    // let p = new Promise(function (resolve, reject) {
    //   //做一些异步操作
    //   setTimeout(function () {
    //     console.log('执行完成')
    //     resolve('随便什么数据')
    //   }, 2000)
    // })

    let z = new Promise(function(resolve, reject) {

      setTimeout(() => {
        console.log('执行完成')
        reject('随便什么数据')
      }, 2000)
    })
    console.log(z)
    z.then(() => {
      console.log("then")
    }).catch(() => {
      console.log("catch")
    }).finally(() => {
      console.log('finally')
    })


    // console.log(p)

    //   p.then(res => {
    //     console.log('then')
    //   }).catch(res=>{
    //     console.log('catch')
    //   }).finally(res=>{
    //     console.log('finally')
    //   })
  },
  searchweather(){
    var z=this;
    wx.request({
      url: 'https://ip.tianqiapi.com/',
      data:{
       
      },
      method:'post',
      header: {
        'content-type': 'application/x-www-form-urlencoded'
      },
      success:function(res){
        console.log(res);
        z.data.weatherip=res.data.ip
        console.log(z.data.weatherip);
      }
    })

    this.weatherdetail(z.data.weatherip)
    

  },
  weatherdetail(ip){
    var z = this;
    wx.request({
      url: 'https://www.tianqiapi.com/api/?version=v6',
      data:{
        'ip':ip
      },
      method: 'GET',
      header: {
        'content-type': 'application/x-www-form-urlencoded'
      },
      success:function(res){
        console.log(res);
        z.setData({
          weather:res.data
        }, () =>{
          console.log(z.data.weather);
        })
      }
    })
   
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})