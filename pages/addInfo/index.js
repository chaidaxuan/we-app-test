// pages/addInfo/index.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    name:'',
    num:'',
    
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log(options)
    let info = options.info
   

  },
  selectImage(e) {
    const z = this
    let { key } = e.currentTarget.dataset
    wx.chooseImage({
      count: 1,
      sizeType: ['compressed'],
      success: function(res) {
        // upload - success - path

        let json = {}

        json[key] = res.tempFilePaths[0]
        z.setData(json)
      },
    })
  },
  inputChange(e) {
    let { value } = e.detail
    let {key} = e.currentTarget.dataset

    let json = {}

    json[key] = value

    console.log(json)

    this.setData(json)
  },
  confirm() {
    let {
      name, num, image0, image1
    } = this.data

    let json = {
      name, num, image0, image1
    }
    console.log(json)
  }
})