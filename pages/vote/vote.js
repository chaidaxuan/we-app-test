// pages/vote/vote.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    isvoted: [{
        type: false,
        image: '/images/girl.png',
      introductiondetail: '0这张是悦诗风吟培训部2019\n年中区区域一季度ttt的合影这张是\n悦诗风吟培训部2019年中区区域一季\n度ttt的合影这张是悦诗风吟培训部2019年中区区域一季度ttt的合影这张是悦诗风吟培训部2019年中区区域一季度ttt的合影',
        show: false
      },
      {
        type: false,
        image: '/images/girl.png',
        introductiondetail: '1这张是悦诗风吟培训部2019年中区区域一季度ttt的合影这张是悦诗风吟培训部2019年中区区域一季度ttt的合影这张是悦诗风吟培训部2019年中区区域一季度ttt的合影这张是悦诗风吟培训部2019年中区区域一季度ttt的合影',
        show: false
      },
      {
        type: false,
        image: '/images/girl.png',
        introductiondetail: '2这张是悦诗风吟培训部2019年中区区域一季度ttt的合影这张是悦诗风吟培训部2019年中区区域一季度ttt的合影这张是悦诗风吟培训部2019年中区区域一季度ttt的合影这张是悦诗风吟培训部2019年中区区域一季度ttt的合影',
        show: false
      },
      {
        type: false,
        image: '/images/girl.png',
        introductiondetail: '3这张是悦诗风吟培训部2019年中区区域一季度ttt的合影这张是悦诗风吟培训部2019年中区区域一季度ttt的合影这张是悦诗风吟培训部2019年中区区域一季度ttt的合影这张是悦诗风吟培训部2019年中区区域一季度ttt的合影',
        show: false
      }
    ],
    totalvote: 0,
  },

  votesucces(e) {
    console.log(e)
    var z = this
    var totalvote = z.data.totalvote
    var {
      key
    } = e.currentTarget.dataset
    var selectedtodo = 'isvoted[' + key + '].type'

    let {
      type
    } = z.data.isvoted[key]

    if (totalvote == 2 && !type) {
      wx.showToast({
        title: '你已经投满2票了，不可以再投',
        image:'/images/warn.png',
        icon: 'fail',
        duration: 3000
      })
      return
    }

    z.setData({
      [selectedtodo]: !type,
      totalvote: totalvote + (type ? -1 : 1),
    })
    console.log(z.data.totalvote)

    console.log(z.data.isvoted[key].type)
  },
  viewall(e) {
    var z = this
    console.log(e)

    var {
      key
    } = e.currentTarget.dataset
    console.log(key)
    var selectedtodo1 = 'isvoted[' + key + '].show'
    // isvoted[index].show
    z.setData({
      [selectedtodo1]: true
    })
  },
  submit() {
    var z = this
    var votelength = z.data.isvoted.length
    console.log(votelength)
    let votedarray = []
    for (let i = 0; i < votelength; i++) {
      if (z.data.isvoted[i].type == true) {
        votedarray.push(i)
      }
    }
    console.log(votedarray)
  },
})