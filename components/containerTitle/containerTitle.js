// components/container/container.js
const app = getApp()

Component({
    properties: {
        title: {
            type: String,
            value: ''
        },
        hasBottomHandler: {
            type: Boolean,
            value: false
        },
        hasTop: {
            type: Boolean,
            value: false
        },
        bottomAni: {
            type: Object,
            value: {}
        },
        navTextStyle:{
            type:String,
            value:'white'
        },
        bg: {
            type: String,
            value: null
        }
    },
    options: {
        multipleSlots: true // 在组件定义时的选项中启用多slot支持
    },
    externalClasses: ['container-class', 'single-btn', 'bottom-handler-class'],
    data: {
        rate: 1
    },
    ready() {
        let {
            statusBarHeight,
            isPX,
            navBarHeight,
            rate
        } = app.globalData;

        this.setData({
            isPX,
            statusBarHeight,
            navBarHeight,
            rate
        })
        // if(this.data.navTextStyle=='black') {
        //     wx.setBackgroundTextStyle({

        //     })
        // }
    },
    methods: {
        pageScrollHandler(e) {
            console.log('pageScrollHandler in component')

            const z = this
            console.log(41.5 * this.data.rate)
            if (e.scrollTop * this.data.rate >= 83) {
                console.log('title')
                wx.setNavigationBarTitle({
                    title: z.data.title
                })
            } else {
                wx.setNavigationBarTitle({
                    title: ''
                })
            }
        }
    }
})