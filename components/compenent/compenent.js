// compenent.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {

  },

  /**
   * 组件的初始数据
   */
  data: {
    image: '',
    buttonshow: 'true',
    reshoot: ''
  },

  /**
   * 组件的方法列表
   */
  methods: {
    uploadfile() {
      var that = this
      wx.chooseImage({
        count: 1,
        sizeType: ['original', 'compressed'],
        sourceType: ['album', 'camera'],
        success(res) {
          // tempFilePath可以作为img标签的src属性显示图片
          var tempFilePaths = res.tempFilePaths
          that.setData({
            image: tempFilePaths[0],
            buttonshow: false,
            reshoot: true
          })
          // this.data.images = images
          // console.log(tempFilePaths)
            that.triggerEvent('myevent', { image: tempFilePaths[0] });
        }
      })
     
      // this.data.images = images
      // console.log(tempFilePaths)
    },
  },
  formSubmit(e) {
    var that = this
    let json = {
      drivername: e.detail.value.drivername,
      licensenumber: e.detail.value.licensenumber,
      image: that.data.image
    }
    console.log(json)
  },
})
