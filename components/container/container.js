// components/container/container.js
var app = getApp()

Component({
    properties: {
        hasBottomHandler: {
            type: Boolean,
            value: false
        },
        hasTop: {
            type: Boolean,
            value: false
        },
        bottomAni: {
            type: Object,
            value: {}
        },
        navTextStyle:{
            type:String,
            value:'white'
        },
        bg: {
            type: String,
            value:'false'
        }
    },
    options: {
        multipleSlots: true // 在组件定义时的选项中启用多slot支持
    },
    externalClasses: ['container-class', 'single-btn', 'bottom-handler-class'],
    data: {},
    methods: {},
    ready() {
        let {
            statusBarHeight,
            isPX,
            navBarHeight
        } = app.globalData;

        this.setData({
            isPX,
            statusBarHeight,
            navBarHeight,
        })
        // if(this.data.navTextStyle=='black') {
        //     wx.setBackgroundTextStyle({

        //     })
        // }

        // let{
        //   statusBarHeight,
        //   isPX,
        //   navBarHeight
        // }= app.globalData

        // this.setData({
        //   isPX,
        //   statusBarHeight,
        //   navBarHeight,
        // })
    }
})