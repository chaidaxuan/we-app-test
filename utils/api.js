const network = require('./network-wrap.js')
const loading = require('./loading.js')

const baseUrl = 'https://www.easy-mock.com/mock/5b9b22636a29d2427a5d90a6/apitest'

const doc = {
    book: {
        getList: "/book/getList",
    },
}

function makeUrl(apiUrl) {
    return baseUrl + apiUrl
}

function makeUrlMap(map) {
    let obj = {}
    for (let key in map) {
        let url = makeUrl(map[key])
        /**
         * options:{
         *      type, post, get, upload
         *      data,
         *      success,
         *      fail,
         * }
         */
        obj[key] = function(options) {
            if (!options) {
                options = {}
            }
            if (!options.data) {
                options.data = {}
            }

            let showLoading = true
            let showFail = true
            let needLogin = true
            if (options.showLoading != undefined) {
                showLoading = options.showLoading
            }
            if (options.showFail != undefined) {
                showFail = options.showFail
            }
            if (options.needLogin != undefined) {
                needLogin = options.needLogin
            }
            if (showLoading) {
                loading.show()
            }

            const app = getApp()
            if (app.globalData.loginData) {
                Object.assign(options.data, app.globalData.loginData)
            }
            let type = options.type || 'post'
            delete options.type

            let tryCount = 0

            Object.assign(options, {
                url,
                complete: function(res) {
                    if (showLoading) {
                        loading.hide()
                    }

                    let info = null
                    if (res.statusCode == 200) {
                        if (res.data.code > 1000 && res.data.data) {
                            info = res.data.data
                            // 未登录
                            if (res.data.code == 1101 && needLogin) {
                                console.log('needLogin')
                                // const app = getApp()
                                // app.handle(() => {
                                //     if (tryCount == 0) {
                                //         if (app.globalData.loginData) {
                                //             Object.assign(options.data, app.globalData.loginData)
                                //         }
                                //         network[type](options).then(res => {
                                //             if (options.success) {
                                //                 options.success(res)
                                //             }
                                //         }).catch(res => {
                                //             if (options.fail) {
                                //                 options.fail(res)
                                //             }
                                //         })
                                //         tryCount++
                                //     }
                                // })
                                // return
                            }
                        }
                    } else {
                        if (res.data) {
                            info = res.data
                        } else {
                            info = res
                        }
                    }
                    if (info && showLoading && showFail) {
                        console.log('fail')
                        loading.fail(info)
                    }
                }
            })

            return network[type](options)
        }
    }
    return obj
}

module.exports = Object.assign({
    serverUrl: baseUrl,
}, (() => {
    let obj = {}
    for (let key in doc) {
        obj[key] = makeUrlMap(doc[key])
    }
    return obj
})())