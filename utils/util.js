let app

const formatTime = date => {
    const year = date.getFullYear()
    const month = date.getMonth() + 1
    const day = date.getDate()
    const hour = date.getHours()
    const minute = date.getMinutes()
    const second = date.getSeconds()

    return [year, month, day].map(formatNumber).join('-') + ' ' + [hour, minute, second].map(formatNumber).join(':')
}

const formatNumber = n => {
    n = n.toString()
    return n[1] ? n : '0' + n
}

function regCheck(reg, str) {
    var re = new RegExp(reg);
    if (re.test(str)) {
        return true;
    } else {
        return false;
    }
}

const isEmptyString = (str) => {
    return str == null || str == undefined || typeof(str) == 'string' && str == ''
}

function isObject(obj) {
    return obj !== null && typeof obj === 'object'
}

function isNull(p) {
    return p == null || p == undefined
}

function isCN(str) {
    return regCheck(/^[\u4e00-\u9fa5]*$/, str)
}

function isMobile(mobile) {
    return regCheck(/^1\d{10}$/, mobile)
}

function isID(str) {
    return regCheck(/(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/, str)
}

// 1111-11-11 11:11:11
function dateFromString(time) {
    if (time == null) {
        return ''
    }
    time = time.replace(/-/g, ':').replace(' ', ':')
    time = time.split(':')
    var time1 = new Date(time[0], (time[1] - 1), time[2], time[3], time[4], time[5])
    return time1
}

function dateDiffInDays(a, b) {
    const _MS_PER_DAY = 1000 * 60 * 60 * 24;
    // Discard the time and time-zone information.
    const utc1 = Date.UTC(a.getFullYear(), a.getMonth(), a.getDate());
    const utc2 = Date.UTC(b.getFullYear(), b.getMonth(), b.getDate());

    return Math.floor((utc2 - utc1) / _MS_PER_DAY);
}

function cmpDate(a, b) {
    if (typeof(a) == 'string') {
        a = dateFromString(a)
    }
    if (typeof(b) == 'string') {
        b = dateFromString(b)
    }

    return a > b
}

// yyyy/mm/dd
function ymdFormat(date, sep = '/') {
    if (date == null) {
        return ''
    }
    if (!isObject(date)) {
        date = dateFromString(date)
    }
    const year = date.getFullYear()
    let month = date.getMonth() + 1
    let day = date.getDate()

    month = month < 10 ? '0' + month : month
    day = day < 10 ? '0' + day : day

    return [year, month, day].join(sep)
}

function ymdCNFormat(date) {
    if (date == null) {
        return ''
    }
    if (!isObject(date)) {
        date = dateFromString(date)
    }
    const year = date.getFullYear()
    const month = date.getMonth() + 1
    const day = date.getDate()

    return `${year}年${month}月${day}日`
}

function ymdhCNFormat(date) {
    if (date == null) {
        return ''
    }
    if (!isObject(date)) {
        date = dateFromString(date)
    }
    const year = date.getFullYear()
    const month = date.getMonth() + 1
    const day = date.getDate()
    const hour = date.getHours()
    const minute = date.getMinutes()

    return `${year}年${month}月${day}日 ${hour}:${minute}`
}

function showModal(content, title = '提示', callback) {
    if (isNull(content)) {
        content = ''
    } else if (isObject(content)) {
        try {
            content = JSON.stringify(content)
        } catch (e) {
            console.log(e)
            content = typeof(content)
        }
    }

    content = content + ''
    content = content.substring(0, 499)

    wx.showModal({
        title: title,
        content: content,
        showCancel: false,
        complete: function(res) {
            if (callback) {
                callback(res)
            }
        }
    })
}

function handleFail(res, content = '网络出错', callback) {
    console.log('handleFail:')
    console.log(res)
    if (res.data) {
        content = res.data
    } else if (res instanceof Error) {
        content = res.toString()
    } else if (typeof(res) == 'object') {
        try {
            content = content + JSON.stringify(res)
        } catch (e) {
            console.log(e)
        }
    }

    content = content + ''
    content = content.substring(0, 499)
    showModal('提示', content, callback)
}

// func
function throttle(fn, gapTime) {
    if (gapTime == null || gapTime == undefined) {
        gapTime = 1500
    }

    let _lastTime = null;
    return function() {
        let _nowTime = +new Date()
        if (_nowTime - _lastTime > gapTime || !_lastTime) {
            fn.apply(this, arguments);
            _lastTime = _nowTime
        }
    }
}

function encodeUrlJson(json) {
    let result = null
    try {
        result = encodeURIComponent(JSON.stringify(json))
    } catch (e) {
        console.log(e)
        result = JSON.stringify({})
    }
    return result
}

function decodeUrlJson(str) {
    let result = null
    try {
        result = JSON.parse(decodeURIComponent(str))
    } catch (e) {
        console.log(e)
        result = JSON.parse(JSON.stringify({}))
    }
    return result
}

function previewFile(url) {
    wx.getSavedFileList({
        complete: res => {
            console.log(res)
        }
    })
    // return
    let exts = ['jpg', 'jpeg', 'png', 'gif']
    let ext = url.split('.').pop()
    if (exts.indexOf(ext) < 0) {
        wx.showLoading({
            title: '正在下载...',
            mask: true
        })
        wx.downloadFile({
            url: url,
            // url: app.api.serverUrl + '/files/static/保密制度.pdf',
            success: res => {
                wx.hideLoading()
                var filePath = res.tempFilePath

                console.log(filePath)
                wx.openDocument({
                    filePath: filePath,
                    success: function(res) {
                        // console.log('打开文档成功')
                    },
                    fail: res => {
                        showToast('打开文档失败')
                    }
                })
            },
            fail: res => {
                wx.hideLoading()
                showToast('下载失败')
            }
        })
    } else {
        wx.previewImage({
            urls: [url],
        })
    }
}

function showToast(title) {
    wx.showToast({
        title,
        image: '/assets/images/iconfont-toptips.png'
    })
}

function floorFix(number, fix) {
    const pos = Math.pow(10, fix)
    return Math.floor(number * pos) / pos
}

function validateObj(obj) {
    let isValid = true
    for (let key in obj) {
        if (isEmptyString(obj[key])) {
            isValid = false
            break
        }
    }
    return isValid
}

// str*n
function repeat(str, n) {
    return new Array(n + 1).join(str);
}

function encodeString(str) {
    if (!str) {
        return ''
    }
    if (str.length > 6) {
        var pre = str.substring(0, 3)
        var sub = str.substring(str.length - 3)

        var cen = repeat('*', str.length - 6)

        return pre + cen + sub
    }
    return str
}

let windowWidth = null

function rpx2px(rpx) {
    if (windowWidth == null) {
        windowWidth = app.globalData.systemInfo.windowWidth
    }
    return windowWidth / 750 * rpx;
}

function px2rpx(px) {
    if (windowWidth == null) {
        windowWidth = app.globalData.systemInfo.windowWidth
    }
    return px / (windowWidth / 750)
}

function hslToRgb(h, s, l) {
    var r, g, b;
    if (s == 0) {
        r = g = b = l; // achromatic
    } else {
        function hue2rgb(p, q, t) {
            if (t < 0) t += 1;
            if (t > 1) t -= 1;
            if (t < 1 / 6) return p + (q - p) * 6 * t;
            if (t < 1 / 2) return q;
            if (t < 2 / 3) return p + (q - p) * (2 / 3 - t) * 6;
            return p;
        }

        var q = l < 0.5 ? l * (1 + s) : l + s - l * s;
        var p = 2 * l - q;

        r = hue2rgb(p, q, h + 1 / 3);
        g = hue2rgb(p, q, h);
        b = hue2rgb(p, q, h - 1 / 3);
    }

    return [r * 255, g * 255, b * 255];
}

// https://stackoverflow.com/questions/542938/how-do-i-get-the-number-of-days-between-two-dates-in-javascript
function parseDate(str) {
    var ymd = str.split('-');
    return new Date(ymd[0], ymd[1] - 1, ymd[2]);
}

function datediff(first, second) {
    // console.log(first, second)
    // Take the difference between the dates and divide by milliseconds per day.
    // Round to nearest whole number to deal with DST.
    console.log((second - first) / (1000 * 60 * 60 * 24))
    return Math.round((second - first) / (1000 * 60 * 60 * 24));
}
function datediffv2(first, second) {
    return (second - first) / (1000 * 60 * 60 * 24)
}

function beforeTime(beforedata) {
  let date = new Date()
  let ms = date.getTime() - beforedata.getTime()
  let s = ms / 1000
  let minute = parseInt(s / 60)
  s = s / 60
  let hour = parseInt(s / 60)
  s = s / 60
  let day = parseInt(s / 24)
  s = Math.round(s / 24)
  let before
  if (day >= 1) {
    before = `${day}天前`
  } else if (hour >= 1) {
    before = `${hour}小时前`
  } else if (minute >= 1) {
    before = `${minute}分钟前`
  } else {
    before = ''
  }
  return before
}

function fixDay(day) {
  return +day < 10 ? `0${day}` : day
}

function initUtil(_app) {
    app = _app
}

module.exports = {
    initUtil,
    formatTime,
    ymdFormat,
    handleFail,
    showModal,
    throttle,
    isEmptyString,
    isNull,
    encodeUrlJson,
    decodeUrlJson,
    previewFile,
    dateFromString,
    cmpDate,
    dateDiffInDays,
    showToast,
    floorFix,
    validateObj,
    isCN,
    isID,
    encodeString,
    rpx2px,
    px2rpx,
    isMobile,
    ymdCNFormat,
    ymdhCNFormat,
    hslToRgb,
    parseDate,
    datediff,
    fixDay,
    beforeTime,
    datediffv2
}